var yaml = require("node-yaml");
var express = require('express');
var bodyParser = require('body-parser');


module.exports = {

  express: null,
  controller: null,

  routes: null,

  init() {
    var that = this;

    this.express = express();
    this.express.listen(PARAMETERS.PORT);
    this.express.use(bodyParser.json());

    this.controller = CONTROLLER;

    this.setRoutes(function(){
      that.parseRoutes();
      that.setNotFound();
    });
  },

  setRoutes: function(yml_loaded) {
    var that = this;
    yaml.read(PARAMETERS.ROUTING_YML_FILE,PARAMETERS.ROUTING_YML_ENCODING,function(err, data) {
      that.routes = data.routes
      yml_loaded();
    });
  },

  parseRoutes: function() {
    var that = this;
    for (r in that.routes) {
      that.router(that.routes[r].method,that.routes[r].path)
    }
  },

  router: function(method,path) {
    var that = this;
    var method = method.toLowerCase();
    var route = that.getRouteByPath(path)
    var functionName = method + route.controller.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); }); //capitalize

    that.express[method](path,function(request, response) {

      var header=request.headers['authorization']||'',        // get the header
      token=header.split(/\s+/).pop()||'',            // and the encoded auth token
      auth=new Buffer(token, 'base64').toString(),    // convert from base64
      parts=auth.split(/:/),                          // split on colon
      username=parts[0],
      password=parts[1];

      if ( PARAMETERS.API_KEYS.indexOf(token) == -1 && !route.public){
        that.createResponse(response,401,JSON.stringify({text:"Unauthorized"}),"text/json")
      } else {
        that.controller[functionName](request, response)
      }
    });
  },

  setNotFound: function() {
    var that = this;
    this.express.use(function(request, response, next){
      that.createResponse(response,404,JSON.stringify({text:"Not found"}),"text/json")
    });
  },

  // return a json response with code and content
  createResponse :  function  (response,code,content,content_type) {
    response.writeHead(code, {"Content-Type": content_type});
    response.write(content);
    response.end();
  },

  getRouteByPath: function(path) {
    var that = this;
    for (r in that.routes) {
      if (that.routes[r].path == path) {
        return that.routes[r]
      }
    }
    return false;
  }

}
