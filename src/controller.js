
module.exports = {


  getTest_route_1: function(request, response) {
    var json = JSON.stringify(request.query)
    return API.createResponse(response,200,json,"text/json");
  },

  postTest_route_2: function(request, response) {
    console.log("body",request.body)
    console.log('controller POST')
    var json = JSON.stringify({text:"test_route222!!!!!!!!!!!!!"})
    return API.createResponse(response,200,json,"text/json");
  },

  putTest_route_3: function(request, response) {
    console.log("body",request.body)
    console.log('controller PUT')
    var json = JSON.stringify({text:"test_route_3!!!!!!!!!!!!!"})
    return API.createResponse(response,200,json,"text/json");
  },

  deleteTest_route_4: function(request, response) {
    console.log('controller DELETE')
    var json = JSON.stringify({text:"test_route_4!!!!!!!!!!!!!"})
    return API.createResponse(response,200,json,"text/json");
  },

}
