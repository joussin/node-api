
global.PARAMETERS = {
  PORT : 8080,
  ROUTING_YML_FILE :  "./src/routing.yml",
  ROUTING_YML_ENCODING : "utf8",
  API_KEYS: [
    "c3RlZjoxMjMxMjM=" // stef:123123 in base64
  ]
}

global.API  = require('./api.js');
global.CONTROLLER = require('./src/controller.js');


API.init();
